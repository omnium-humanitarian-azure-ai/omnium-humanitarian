# OMNIUM Humanitarian #



### What is OMNIUM Humanitarian? ###

**OMNIUM** (**O**verall **M**etrics & **N**etworking of **I**deas **U**nified for the **M**asses) **Humanitarian** 
is an **Azure AI** project that addresses the humanitarian issues of 
**Disaster response**, **Refugee and displaced people**, **Human rights**, and the **Needs of women and children**. 